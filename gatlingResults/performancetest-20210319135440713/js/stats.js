var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "252",
        "ok": "252",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "549",
        "ok": "549",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "109",
        "ok": "109",
        "ko": "-"
    },
    "percentiles1": {
        "total": "565",
        "ok": "565",
        "ko": "-"
    },
    "percentiles2": {
        "total": "604",
        "ok": "604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "743",
        "ok": "743",
        "ko": "-"
    },
    "percentiles4": {
        "total": "869",
        "ok": "869",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 244,
        "percentage": 97
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 8,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "4.667",
        "ok": "4.667",
        "ko": "-"
    }
},
contents: {
"req_create-article--e6da5": {
        type: "REQUEST",
        name: "Create article: First title",
path: "Create article: First title",
pathFormatted: "req_create-article--e6da5",
stats: {
    "name": "Create article: First title",
    "numberOfRequests": {
        "total": "84",
        "ok": "84",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "324",
        "ok": "324",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "541",
        "ok": "541",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "110",
        "ok": "110",
        "ko": "-"
    },
    "percentiles1": {
        "total": "559",
        "ok": "559",
        "ko": "-"
    },
    "percentiles2": {
        "total": "594",
        "ok": "594",
        "ko": "-"
    },
    "percentiles3": {
        "total": "687",
        "ok": "687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "891",
        "ok": "891",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 81,
        "percentage": 96
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 3,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.556",
        "ok": "1.556",
        "ko": "-"
    }
}
    },"req_create-article--6b714": {
        type: "REQUEST",
        name: "Create article: Second title",
path: "Create article: Second title",
pathFormatted: "req_create-article--6b714",
stats: {
    "name": "Create article: Second title",
    "numberOfRequests": {
        "total": "84",
        "ok": "84",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "323",
        "ok": "323",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "921",
        "ok": "921",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "562",
        "ok": "562",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles1": {
        "total": "576",
        "ok": "576",
        "ko": "-"
    },
    "percentiles2": {
        "total": "610",
        "ok": "610",
        "ko": "-"
    },
    "percentiles3": {
        "total": "748",
        "ok": "748",
        "ko": "-"
    },
    "percentiles4": {
        "total": "856",
        "ok": "856",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 80,
        "percentage": 95
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 4,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.556",
        "ok": "1.556",
        "ko": "-"
    }
}
    },"req_create-article--16a38": {
        type: "REQUEST",
        name: "Create article: Third title",
path: "Create article: Third title",
pathFormatted: "req_create-article--16a38",
stats: {
    "name": "Create article: Third title",
    "numberOfRequests": {
        "total": "84",
        "ok": "84",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "819",
        "ok": "819",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "544",
        "ok": "544",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles1": {
        "total": "563",
        "ok": "563",
        "ko": "-"
    },
    "percentiles2": {
        "total": "601",
        "ok": "601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "739",
        "ok": "739",
        "ko": "-"
    },
    "percentiles4": {
        "total": "800",
        "ok": "800",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 83,
        "percentage": 99
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.556",
        "ok": "1.556",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
